$(document).ready(function() {
	// general nav route
	Hasher.add(':page', function(page) {
		$('#nav a[href="#' + page + '"]').tab('show');
	});
	
	// Setup the Hasher
	Hasher.setup();
	
	// prettify
	prettyPrint();

	// heyGirl image slider
	$('#heyGirlCarousel').carousel({ interval: 10000 });

	// random heyGirl dynamic loader
	$('#heyGirlCarousel .item').each(function () {
		var tempItem = this;
		$.getJSON('http://api.lovefromryan.com/heyGirl', function(json) {	
			if (json.meta.status === 200) {
				$(tempItem).find('.carousel-caption p').text(json.response.data[0].heyGirl);
				$(tempItem).find('.label').text(json.response.data[0].type);
			}
		});
	});
	
});